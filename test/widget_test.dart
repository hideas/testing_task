import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:todo/main.dart';
import 'package:todo/widgets/todo_list_widget.dart';

void main() {
  testWidgets('Widgets test', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    await tester.pumpWidget(TodoList());    

    // Tap the '-' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.remove));
    await tester.pump();

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();
  });
}
