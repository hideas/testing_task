import 'dart:math';
import 'package:flutter/material.dart';

// Class for mutable widget
class TodoList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TodoListState();
  }
}

// Class for the state
class TodoListState extends State<TodoList> {
  // Randomizer from dart:math
  Random random = Random();

  // List of items - generate 20 by default
  final List<String> items = List<String>.generate(20, (int i) => 'Item ${i + 1}');

  // List of fonts
  final List<String> fonts = <String>['RobotoCondensed', 'RobotoMono', 'RobotoSlab'];

  // Make Scaffold with two FABs
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Todo list')),
      body: _buildList(),
      floatingActionButton: Stack(
         // FAB for remove method
        children: <Widget>[
          Padding(padding: const EdgeInsets.only(left:31),
          child: Align(
            alignment: Alignment.bottomLeft,
            child: FloatingActionButton(
              onPressed: () => _removeItem(context),
              child: Icon(Icons.remove),
            ),
          ),
          ),
          // FAB for add method
          Align(
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              onPressed: () => _addNewItem(context),
              tooltip: 'Add',
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
    );
  }

  // List builder with Dismissible for items
  Widget _buildList() {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (BuildContext ctxt, int index) {
        final String item = items[index];
        return Dismissible(
          key: Key(item),
          onDismissed: (DismissDirection direction) {
            setState(() {
              items.removeAt(index);
            });
            // Scaffold.of(context).showSnackBar(SnackBar(content: Text('$item dismissed')));
          },
          background: Container(
            alignment: AlignmentDirectional.centerEnd,
            color: Colors.teal,
            child: Icon(
              Icons.delete,
              color: Colors.white,
            ),
          ),
          // Ink widget with randomizer for background color
          child: Ink(
            color: Color((random.nextDouble() * 0xFFFFFF).toInt() << 0).withOpacity(1.0),
            child: ListTile(
              title: Text(items[index],
                style: TextStyle(
                  fontFamily: fonts[random.nextInt(3)],
                  fontWeight: FontWeight.bold,
                ),
              ),
              // PopupMenuButton to delete specific item
              trailing: PopupMenuButton<dynamic>( 
                onSelected: _onSelected,
                icon: Icon(Icons.menu),
                color: Colors.teal,
                itemBuilder: (BuildContext context) => <PopupMenuEntry<dynamic>>[
                  PopupMenuItem<dynamic>(
                    value: items[index],
                    child: const Text('Delete'),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  // TextEditingController for Alert
  final TextEditingController _textFieldController = TextEditingController();

  // Method for remove items in the list
  void _removeItem(BuildContext context) {
    showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Enter item name'),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: 'Item name to remove'),
          ),
          actions: <Widget>[
            FlatButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Remove'),
              onPressed: () {
                setState(() {
                  if (_textFieldController.text.isNotEmpty) {
                    items.remove(_textFieldController.text);
                    _textFieldController.clear();
                  }
                });
              Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
    );
  } 

  // Method for add items in the list
  void _addNewItem(BuildContext context) {
    showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Enter item name'),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: 'Item name to add'),
          ),
          actions: <Widget>[
            FlatButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Add'),
              onPressed: () {
                setState(() {
                  if (_textFieldController.text.isNotEmpty) {
                    items.add(_textFieldController.text);
                    _textFieldController.clear();
                  }
                });
              Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
    );
  }

  void _onSelected(dynamic val) {
    setState(() => items.removeWhere((String data) => data == val));
  }
}
